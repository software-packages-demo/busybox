# busybox

Tiny utilities for small and embedded systems. https://busybox.net/

# See also
* [*BusyBox*](https://en.wikipedia.org/wiki/BusyBox)
* gitlab.com/[hub-docker-com-demo/alpine](https://gitlab.com/hub-docker-com-demo/alpine)
* gitlab.com/[apk-packages-demo/busybox](https://gitlab.com/apk-packages-demo/busybox)
* gitlab.com/[apt-packages-demo/busybox](https://gitlab.com/apt-packages-demo/busybox)
* gitlab.com/[dnf-packages-demo/busybox](https://gitlab.com/dnf-packages-demo/busybox)
* gitlab.com/[xbps-packages-demo/busybox](https://gitlab.com/xbps-packages-demo/busybox)

# Applets
## `init`
* [busybox init](https://www.google.com/search?q=busybox+init)
* Section [*BusyBox init*
  ](https://subscription.packtpub.com/book/networking_and_servers/9781784392536/9/ch09lvl1sec97/busybox-init)
  * [Mastering Embedded Linux Programming](https://www.worldcat.org/search?q=Mastering+Embedded+Linux+Programming)
  * Ch. 9 "Starting up - the init Program"
* [man busybox /etc/inittab](https://google.com/search?q=man+busybox+/etc/inittab)
* path: [root/examples/inittab](https://git.busybox.net/busybox/tree/examples/inittab)
  * Contains default behavior:
    ```
	::sysinit:/etc/init.d/rcS
	::askfirst:/bin/sh
	::ctrlaltdel:/sbin/reboot
	::shutdown:/sbin/swapoff -a
	::shutdown:/bin/umount -a -r
	::restart:/sbin/init
	tty2::askfirst:/bin/sh
	tty3::askfirst:/bin/sh
	tty4::askfirst:/bin/sh
    ```
* Note: Alpine `/etc/inittab` is in package alpine-baselayout
  it is not clear at the moment if it is designed for
  BusyBox or OpenRC (openrc-init).
* OpenRC [Example inittab compatible with Busybox init
  ](https://wiki.gentoo.org/wiki/OpenRC)
  ```
  ::sysinit:/sbin/openrc sysinit
  ::wait:/sbin/openrc boot
  ::wait:/sbin/openrc
  ```
## `time` bug
* `busybox time` may never return if the timed executable does not exist. (2020-06 with Alpine Linux)

# License
* GPLv2
* [*BusyBox is licensed under the GNU General Public License, version 2*
  ](https://busybox.net/license.html)

# Documentation useful for using Busybox
* [*Sed - An Introduction and Tutorial*
  ](http://www.grymoire.com/Unix/Sed.html) Bruce Barnett 2015